package com.itrocket.welcomebot.facenetdetection.infrastructure.container

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.itrocket.welcomebot.facenetdetection.R
import com.itrocket.welcomebot.facenetdetection.faceRecognition.service.BackgroundCameraService

class ContainerActivity : AppCompatActivity(R.layout.activity_container) {

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                startService(Intent(this, BackgroundCameraService::class.java))
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            startService(Intent(this, BackgroundCameraService::class.java))
        } else {
            requestPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }
}