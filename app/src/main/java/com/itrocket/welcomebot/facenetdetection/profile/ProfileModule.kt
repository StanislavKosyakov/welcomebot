package com.itrocket.welcomebot.facenetdetection.profile

import com.itrocket.structure.utils.viewModel
import com.itrocket.welcomebot.facenetdetection.infrastructure.db.DataBase
import com.itrocket.welcomebot.facenetdetection.profile.domain.ProfileInteractor
import com.itrocket.welcomebot.facenetdetection.profile.domain.boundary.ProfileDomain
import com.itrocket.welcomebot.facenetdetection.profile.vm.ProfileRetainedRetainedViewModel
import com.itrocket.welcomebot.facenetdetection.profile.vm.boundary.ProfileViewModel
import com.itrocket.welcomebot.facenetdetection.profiles.domain.models.Profile
import org.koin.dsl.module

object ProfileModule {

    val module = module {

        viewModel<ProfileViewModel> { (profile: Profile?) ->
            ProfileRetainedRetainedViewModel(profile)
        }

        factory<ProfileDomain> {
            ProfileInteractor(get(), get(), get())
        }

        factory {
            get<DataBase>().profileDao()
        }
    }
}