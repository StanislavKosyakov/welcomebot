package com.itrocket.welcomebot.facenetdetection.profile.vm

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.itrocket.structure.vm.BaseRetainedViewModel
import com.itrocket.welcomebot.facenetdetection.infrastructure.utils.toBitmap
import com.itrocket.welcomebot.facenetdetection.profile.domain.boundary.ProfileDomain
import com.itrocket.welcomebot.facenetdetection.profile.vm.boundary.ProfileViewModel
import com.itrocket.welcomebot.facenetdetection.profiles.domain.models.Profile
import com.welcomebot.facerecognition.extractor.FaceNotFoundException
import kotlinx.coroutines.launch
import org.koin.core.inject
import timber.log.Timber


class ProfileRetainedRetainedViewModel(private val profile: Profile?) : BaseRetainedViewModel(),
    ProfileViewModel {

    private val domain: ProfileDomain by inject()

    private val context: Context by inject()

    private var changedName: String? = null

    private var changedFace: Bitmap? = null

    override val photo = MutableLiveData<Bitmap?>()

    override val name = MutableLiveData<String>()

    init {
        launch {
            if (profile != null) {
                val bitmapPhoto =
                    BitmapFactory.decodeFile(profile.photo.absolutePath, BitmapFactory.Options())

                photo.postValue(bitmapPhoto)

                name.postValue(profile.name)
            }
        }
    }

    override val handleException = { exception: Throwable ->
        if (exception is FaceNotFoundException) {
            showException(exception)
        } else {
            Timber.e(exception)
        }
    }

    override fun onPhotoPicked(uri: Uri) {
        launch {
            val photoBitmap = uri.toBitmap(context)

            photoBitmap?.let {
                changedFace = domain.extractFace(photoBitmap)

                photo.postValue(changedFace)
            }
        }
    }

    override fun onNameChanged(name: String) {
        changedName = name
    }

    override fun onSaveClick() {
        launch {
            val newFace = changedFace
            val newName = changedName

            if (profile == null && !newName.isNullOrEmpty() && newFace != null) {
                domain.createProfile(newName, newFace)

                back()
            } else if (profile != null) {
                domain.updateProfile(profile.id, newName, newFace)

                back()
            }
        }
    }
}