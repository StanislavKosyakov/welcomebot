package com.itrocket.welcomebot.facenetdetection.faceRecognition.domain.boundary

import android.graphics.Bitmap
import com.itrocket.welcomebot.facenetdetection.faceRecognition.domain.model.FaceNotRecognizeException
import com.welcomebot.facerecognition.extractor.FaceNotFoundException

interface FaceRecognitionDomain {

    /**
     * Получает все вектора лиц добавленых в приложение
     *
     * @return Map<String, FloatArray> - где ключ это имя для лица, а значение это его вектор
     */
    suspend fun getAllFacesVectors(): Map<String, FloatArray>

    /**
     * Распознает лицо на изображение, для поиска использует переданный в параметры массив векторов
     *
     * @param image Bitmap - входное изображение
     * @param faces Map<String, FloatArray> - хранилище с лицами и векторами
     * @return String - распознанное имя лица
     *
     * @throws @[FaceNotFoundException] - если на входном изображение лицо не обнаружено
     * @throws @[FaceNotRecognizeException] - если лицо не было распознано
     */
    suspend fun recognizeFace(image: Bitmap, faces: Map<String, FloatArray>): String
}