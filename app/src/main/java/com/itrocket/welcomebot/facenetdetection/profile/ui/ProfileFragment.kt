package com.itrocket.welcomebot.facenetdetection.profile.ui

import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import coil.load
import coil.transform.CircleCropTransformation
import com.itrocket.structure.ui.BaseFragment
import com.itrocket.structure.utils.viewModel
import com.itrocket.welcomebot.facenetdetection.R
import com.itrocket.welcomebot.facenetdetection.databinding.FragmentProfileBinding
import com.itrocket.welcomebot.facenetdetection.profile.vm.boundary.ProfileViewModel
import org.koin.core.parameter.parametersOf

class ProfileFragment : BaseFragment(R.layout.fragment_profile) {

    private val viewBinding by viewBinding(FragmentProfileBinding::bind)

    private val args: ProfileFragmentArgs by navArgs()

    private val takePhoto =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            uri?.let {
                viewModel.onPhotoPicked(uri)
            }
        }

    override val viewModel: ProfileViewModel by viewModel {
        parametersOf(args.profile)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding.photo.setOnClickListener {
            takePhoto.launch(IMAGE_MIME_TYPE)
        }

        viewModel.photo.observe(viewLifecycleOwner, { photo ->
            viewBinding.photo.load(photo) {
                transformations(CircleCropTransformation())
                crossfade(true)
            }
        })

        viewModel.name.observe(viewLifecycleOwner, { name ->
            viewBinding.name.setText(name)
        })

        viewBinding.save.setOnClickListener {
            viewModel.onSaveClick()
        }

        viewBinding.name.doAfterTextChanged {
            viewModel.onNameChanged(it.toString())
        }
    }

    companion object {
        private const val IMAGE_MIME_TYPE = "image/*"
    }
}