package com.itrocket.welcomebot.facenetdetection.profile.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.itrocket.welcomebot.facenetdetection.profile.data.db.entity.ProfileEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ProfileDao {

    /**
     * Получает источник данных для списка всех профилей, будет выдавать новый список
     * при каждом обновление сущностей в бд
     *
     * @return Flow<List<ProfileEntity>> - источник данных для списка профилей
     */
    @Query("SELECT * FROM profiles")
    fun getAll(): Flow<List<ProfileEntity>>

    /**
     * Вставляет профиль в базу данных, если такой профиль уже существует заменят его
     *
     * @param profileEntity ProfileEntity
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(profileEntity: ProfileEntity)

    /**
     * Получает профиль по id
     *
     * @param id Int - профиля
     * @return ProfileEntity - профиль
     */
    @Query("SELECT * FROM profiles WHERE id = :id")
    suspend fun get(id: Int): ProfileEntity
}