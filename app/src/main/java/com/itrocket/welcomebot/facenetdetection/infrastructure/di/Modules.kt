package com.itrocket.welcomebot.facenetdetection.infrastructure.di

import com.itrocket.welcomebot.facenetdetection.faceRecognition.FaceRecognitionModule
import com.itrocket.welcomebot.facenetdetection.infrastructure.InfrastructureModule
import com.itrocket.welcomebot.facenetdetection.profile.ProfileModule
import com.itrocket.welcomebot.facenetdetection.profiles.ProfilesModule

object Modules {

    val modules = listOf(
        FaceRecognitionModule.module,
        ProfileModule.module,
        ProfilesModule.module,
        InfrastructureModule.module
    )
}