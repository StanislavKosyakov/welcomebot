package com.itrocket.welcomebot.facenetdetection.profiles.ui.list

import coil.load
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import com.itrocket.welcomebot.facenetdetection.databinding.ItemProfileBinding
import com.itrocket.welcomebot.facenetdetection.profiles.domain.models.Profile

class ProfileAdapter(itemClickedListener: (Profile) -> Unit) :
    AsyncListDifferDelegationAdapter<Profile>(ProfileDiffUtils()) {

    init {
        delegatesManager.addDelegate(createProfileDelegateAdapter(itemClickedListener))
    }
}

fun createProfileDelegateAdapter(itemClickedListener: (Profile) -> Unit) =
    adapterDelegateViewBinding<Profile, Profile, ItemProfileBinding>(
        { layoutInflater, root -> ItemProfileBinding.inflate(layoutInflater, root, false) }
    ) {
        binding.container.setOnClickListener {
            itemClickedListener(item)
        }

        bind {
            binding.name.text = item.name

            binding.photo
                .load(item.photo)
        }
    }