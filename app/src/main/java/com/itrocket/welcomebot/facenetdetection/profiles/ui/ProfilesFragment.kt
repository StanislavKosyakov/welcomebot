package com.itrocket.welcomebot.facenetdetection.profiles.ui

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.itrocket.structure.ui.BaseFragment
import com.itrocket.structure.utils.viewModel
import com.itrocket.welcomebot.facenetdetection.R
import com.itrocket.welcomebot.facenetdetection.databinding.FragmentProfilesBinding
import com.itrocket.welcomebot.facenetdetection.profiles.ui.list.MarginDecorator
import com.itrocket.welcomebot.facenetdetection.profiles.ui.list.ProfileAdapter
import com.itrocket.welcomebot.facenetdetection.profiles.vm.boundary.ProfilesViewModel

class ProfilesFragment : BaseFragment(R.layout.fragment_profiles) {

    override val viewModel: ProfilesViewModel by viewModel()

    private val viewBinding by viewBinding(FragmentProfilesBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupProfiles()

        viewBinding.createProfile.setOnClickListener {
            viewModel.onCreateProfileClick()
        }
    }

    private fun setupProfiles() {
        viewBinding.profiles.layoutManager = LinearLayoutManager(requireContext())

        val adapter = ProfileAdapter { profile ->
            viewModel.onProfileClick(profile)
        }

        viewBinding.profiles.adapter = adapter

        val marginDecorator = MarginDecorator(resources.getDimension(R.dimen.space_16).toInt())

        viewBinding.profiles.addItemDecoration(marginDecorator)

        viewModel.profiles.observe(viewLifecycleOwner, { profiles ->
            adapter.items = profiles
        })
    }
}