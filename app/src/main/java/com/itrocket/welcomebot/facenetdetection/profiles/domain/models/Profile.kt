package com.itrocket.welcomebot.facenetdetection.profiles.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.File

@Parcelize
data class Profile(val id: Int, val name: String, val photo: File) : Parcelable