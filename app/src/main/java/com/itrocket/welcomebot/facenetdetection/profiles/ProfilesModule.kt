package com.itrocket.welcomebot.facenetdetection.profiles

import com.itrocket.structure.utils.viewModel
import com.itrocket.welcomebot.facenetdetection.profiles.domain.ProfilesInteractor
import com.itrocket.welcomebot.facenetdetection.profiles.domain.boudary.ProfilesDomain
import com.itrocket.welcomebot.facenetdetection.profiles.vm.ProfilesRetainedViewModel
import com.itrocket.welcomebot.facenetdetection.profiles.vm.boundary.ProfilesViewModel
import org.koin.dsl.module

object ProfilesModule {

    val module = module {

        viewModel<ProfilesViewModel> {
            ProfilesRetainedViewModel()
        }

        factory<ProfilesDomain> {
            ProfilesInteractor(get())
        }
    }
}