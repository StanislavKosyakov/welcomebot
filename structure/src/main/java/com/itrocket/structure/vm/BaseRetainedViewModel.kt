package com.itrocket.structure.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.itrocket.structure.exception.BaseException
import com.itrocket.structure.utils.Navigation
import com.itrocket.structure.utils.SingleLiveData
import com.itrocket.structure.vm.boundary.BaseViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.koin.core.KoinComponent
import kotlin.coroutines.CoroutineContext

abstract class BaseRetainedViewModel : ViewModel(), BaseViewModel, CoroutineScope, KoinComponent {

    override val navigation = SingleLiveData<Navigation>()

    open val handleException: (exception: Throwable) -> Unit = {}

    override val coroutineContext: CoroutineContext =
        viewModelScope.coroutineContext + Dispatchers.IO + CoroutineExceptionHandler { _, exception ->
            handleException(exception)
        }

    fun showException(baseException: BaseException) {
        navigation.postValue(Navigation.ToException(baseException))
    }

    fun navigate(navDirections: NavDirections) {
        navigation.postValue(Navigation.To(navDirections))
    }

    fun back() {
        navigation.postValue(Navigation.Back)
    }
}