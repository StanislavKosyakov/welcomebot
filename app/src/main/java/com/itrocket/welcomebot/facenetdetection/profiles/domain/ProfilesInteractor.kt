package com.itrocket.welcomebot.facenetdetection.profiles.domain

import com.itrocket.welcomebot.facenetdetection.profile.data.db.dao.ProfileDao
import com.itrocket.welcomebot.facenetdetection.profiles.converter.toProfile
import com.itrocket.welcomebot.facenetdetection.profiles.domain.boudary.ProfilesDomain
import com.itrocket.welcomebot.facenetdetection.profiles.domain.models.Profile
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ProfilesInteractor(private val profileDao: ProfileDao) : ProfilesDomain {

    override suspend fun getProfiles(): Flow<List<Profile>> =
        profileDao
            .getAll()
            .map {
                it.map { profileEntity ->
                    profileEntity.toProfile()
                }
            }
}