package com.itrocket.welcomebot.facenetdetection.infrastructure

import androidx.room.Room
import com.itrocket.welcomebot.facenetdetection.infrastructure.db.DataBase
import org.koin.dsl.module

object InfrastructureModule {

    private const val DATABASE_NAME = "welcomebot.db"

    val module = module {
        single {
            Room.databaseBuilder(get(), DataBase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}