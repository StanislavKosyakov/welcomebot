package com.itrocket.welcomebot.facenetdetection.profile.domain.boundary

import android.graphics.Bitmap

interface ProfileDomain {

    /**
     * Извлекает лицо из изображения
     *
     * @param photo Bitmap - изоражение на котором присутсвует лицо человека
     * @return Bitmap вырезанное из исходного изображения лицо
     */
    suspend fun extractFace(photo: Bitmap): Bitmap

    /**
     * Создает новый профиль
     *
     * @param name String - имя профиля
     * @param face Bitmap - фото профиля
     */
    suspend fun createProfile(name: String, face: Bitmap)

    /**
     * Обновляет профиль
     *
     * @param id Int - профиле
     * @param newName String? - новое имя профиля или null если не изменилось
     * @param newFace Bitmap? - новое лицо для профиля или null если не изменилось
     */
    suspend fun updateProfile(id: Int, newName: String?, newFace: Bitmap?)
}