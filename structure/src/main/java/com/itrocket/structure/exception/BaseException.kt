package com.itrocket.structure.exception

open class BaseException(val title: String, override val message: String) : Exception()