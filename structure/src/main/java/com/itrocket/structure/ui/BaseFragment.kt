package com.itrocket.structure.ui

import android.R
import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.itrocket.structure.exception.BaseException
import com.itrocket.structure.utils.Navigation
import com.itrocket.structure.vm.boundary.BaseViewModel


abstract class BaseFragment(@LayoutRes layout: Int) : Fragment(layout) {

    abstract val viewModel: BaseViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.navigation.observe(viewLifecycleOwner, { navigation ->
            when (navigation) {
                is Navigation.To -> findNavController().navigate(navigation.navDirections)

                Navigation.Back -> findNavController().navigateUp()

                is Navigation.ToException -> showException(navigation.baseException)
            }
        })
    }

    private fun showException(baseException: BaseException) {
        AlertDialog.Builder(activity)
            .setTitle(baseException.title)
            .setMessage(baseException.message)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .create()
            .show()
    }
}