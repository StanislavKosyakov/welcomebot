package com.itrocket.welcomebot.facenetdetection.profiles.converter

import com.itrocket.welcomebot.facenetdetection.profile.data.db.entity.ProfileEntity
import com.itrocket.welcomebot.facenetdetection.profiles.domain.models.Profile
import java.io.File

fun ProfileEntity.toProfile(): Profile =
    Profile(id, name, File(faceAbsoluteFilePath))