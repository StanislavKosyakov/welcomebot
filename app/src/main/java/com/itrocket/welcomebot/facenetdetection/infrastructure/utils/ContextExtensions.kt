package com.itrocket.welcomebot.facenetdetection.infrastructure.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri

/**
 * Извлекает Bitmap из uri
 *
 * @receiver Context - для получения contentResolver
 * @param uri Uri - с изображением
 * @return Bitmap? - вовзрает bitmap изображения или null
 */
fun Uri.toBitmap(context: Context): Bitmap? {
    context.contentResolver?.openInputStream(this)?.use {
        return BitmapFactory.decodeStream(it)
    } ?: return null
}