package com.itrocket.welcomebot.facenetdetection.faceRecognition.domain

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.itrocket.welcomebot.facenetdetection.faceRecognition.domain.boundary.FaceRecognitionDomain
import com.itrocket.welcomebot.facenetdetection.faceRecognition.domain.model.FaceNotRecognizeException
import com.itrocket.welcomebot.facenetdetection.profile.data.db.dao.ProfileDao
import com.itrocket.welcomebot.facenetdetection.profile.data.db.entity.ProfileEntity
import com.welcomebot.facerecognition.extractor.FaceExtractor
import com.welcomebot.facerecognition.model.FaceNetModel
import kotlinx.coroutines.flow.firstOrNull
import timber.log.Timber

class FaceRecognitionInteractor(
    private val profileDao: ProfileDao,
    private val faceNetModel: FaceNetModel,
    private val faceExtractor: FaceExtractor
) : FaceRecognitionDomain {

    override suspend fun getAllFacesVectors(): Map<String, FloatArray> {
        val profiles: List<ProfileEntity>? = profileDao.getAll().firstOrNull()

        return if (profiles == null) {
            emptyMap()
        } else {
            val faces = hashMapOf<String, Bitmap>()

            profiles.forEach { profileEntity ->
                val faceName = profileEntity.id.toString() + " " + profileEntity.name

                faces[faceName] =
                    BitmapFactory.decodeFile(
                        profileEntity.faceAbsoluteFilePath,
                        BitmapFactory.Options()
                    )
            }

            faceNetModel.processImageFaces(faces)
        }
    }

    override suspend fun recognizeFace(image: Bitmap, faces: Map<String, FloatArray>): String {
        val face = faceExtractor.getFace(image)

        val faceVector = faceNetModel.getFaceEmbedding(face)

        var maxSimilarity = 0f
        var recognizedName = ""

        faces.forEach { (name, vector) ->
            val similarity = faceNetModel.cosineSimilarity(vector, faceVector)

            Timber.d("на фото $name с вероятностью $similarity")

            if (similarity > maxSimilarity) {
                maxSimilarity = similarity

                recognizedName = name
            }
        }

        if (maxSimilarity > FaceNetModel.SIMILARITY_THRESHOLD_FACTOR) {
            return recognizedName
        }

        throw FaceNotRecognizeException()
    }
}