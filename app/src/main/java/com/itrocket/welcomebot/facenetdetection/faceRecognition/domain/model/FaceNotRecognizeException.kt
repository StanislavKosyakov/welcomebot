package com.itrocket.welcomebot.facenetdetection.faceRecognition.domain.model

class FaceNotRecognizeException : Exception()