package com.itrocket.welcomebot.facenetdetection.faceRecognition.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import com.itrocket.welcomebot.facenetdetection.R
import com.itrocket.welcomebot.facenetdetection.infrastructure.container.ContainerActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf
import kotlin.coroutines.CoroutineContext

class BackgroundCameraService : Service(), LifecycleOwner, CoroutineScope, KoinComponent {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + SupervisorJob()

    private lateinit var lifecycleRegistry: LifecycleRegistry

    private val imageAnalyzer: ImageAnalysis by inject { parametersOf(this as CoroutineScope) }

    private val cameraSelector: CameraSelector by inject()

    private val context: Context by inject()

    override fun onBind(intent: Intent): IBinder {
        throw UnsupportedOperationException("onBind не был имплементирован")
    }

    override fun onCreate() {
        super.onCreate()

        lifecycleRegistry = LifecycleRegistry(this)

        lifecycleRegistry.currentState = Lifecycle.State.CREATED
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        lifecycleRegistry.currentState = Lifecycle.State.RESUMED

        startForeground()

        setupCamera()

        return super.onStartCommand(intent, flags, startId)
    }

    private fun setupCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(context)

        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()

            cameraProvider.bindToLifecycle(this, cameraSelector, imageAnalyzer)

        }, ContextCompat.getMainExecutor(context))
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        val notificationManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager?

        notificationManager?.let {
            val notificationChannel =
                NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH)

            it.createNotificationChannel(notificationChannel)
        }
    }

    private fun buildNotification(): Notification {
        val pendingIntent: PendingIntent =
            Intent(this, ContainerActivity::class.java).let { notificationIntent ->
                PendingIntent.getActivity(this, 0, notificationIntent, 0)
            }

        return NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(getString(R.string.camera_service_work))
            .setContentText(getString(R.string.camera_service_wait_friends))
            .setContentIntent(pendingIntent)
            .build()
    }

    private fun startForeground() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel()
        }

        startForeground(NOTIFICATION_ID, buildNotification())
    }

    override fun onDestroy() {
        lifecycleRegistry.currentState = Lifecycle.State.DESTROYED

        coroutineContext.cancel()

        super.onDestroy()
    }

    override fun getLifecycle(): Lifecycle =
        lifecycleRegistry

    companion object {
        private const val CHANNEL_ID = "welcome bot camera service"

        private const val CHANNEL_NAME = "welcome bot"

        private const val NOTIFICATION_ID = 1
    }
}