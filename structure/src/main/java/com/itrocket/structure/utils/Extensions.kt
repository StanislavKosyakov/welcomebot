package com.itrocket.structure.utils

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import org.koin.android.viewmodel.ViewModelOwner.Companion.from
import org.koin.android.viewmodel.dsl.setIsViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.definition.BeanDefinition
import org.koin.core.definition.Definition
import org.koin.core.module.Module
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.TypeQualifier

inline fun <reified T : Any> Fragment.viewModel(
    noinline parameters: ParametersDefinition? = null
): Lazy<T> {
    val viewModel = viewModel<ViewModel>(
        qualifier = TypeQualifier(T::class),
        owner = { from(this) },
        parameters = parameters
    )

    return viewModel as Lazy<T>
}

inline fun <reified T : Any> Module.viewModel(
    override: Boolean = false,
    noinline definition: Definition<ViewModel>
): BeanDefinition<ViewModel> {

    val beanDefinition = factory(TypeQualifier(T::class), override, definition)
    beanDefinition.setIsViewModel()
    return beanDefinition
}