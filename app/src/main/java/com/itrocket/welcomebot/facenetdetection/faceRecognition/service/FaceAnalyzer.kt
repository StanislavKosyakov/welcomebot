package com.itrocket.welcomebot.facenetdetection.faceRecognition.service

import android.annotation.SuppressLint
import android.graphics.*
import android.media.Image
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.itrocket.welcomebot.facenetdetection.faceRecognition.domain.boundary.FaceRecognitionDomain
import com.itrocket.welcomebot.facenetdetection.faceRecognition.domain.model.FaceNotRecognizeException
import com.itrocket.welcomebot.facenetdetection.infrastructure.utils.rotate
import com.welcomebot.facerecognition.extractor.FaceNotFoundException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.ByteArrayOutputStream

class FaceAnalyzer(
    private val coroutineScope: CoroutineScope,
    private val faceRecognitionDomain: FaceRecognitionDomain
) : ImageAnalysis.Analyzer {

    private lateinit var faces: Map<String, FloatArray>

    init {
        coroutineScope.launch {
            faces = faceRecognitionDomain.getAllFacesVectors()
        }
    }

    @SuppressLint("UnsafeExperimentalUsageError")
    override fun analyze(image: ImageProxy) {
        if (!::faces.isInitialized) {
            image.close()

            return
        }

        val bitmapImage = image.image ?: run {
            image.close()

            return
        }

        coroutineScope.launch {
            val bitmap = toBitmap(bitmapImage)

            try {
                val rotated = bitmap.rotate(RIGHT_ANGLE)

                val name = faceRecognitionDomain.recognizeFace(rotated, faces)

                Timber.d("на видео потоке найден $name")
            } catch (e: FaceNotFoundException) {
                Timber.d("на видео потоке лицо не найдено")
            } catch (e: FaceNotRecognizeException) {
                Timber.d("на видео потоке лицо найдено, он не распознано")
            }

            image.close()
        }
    }

    private fun toBitmap(image: Image): Bitmap {
        //Извлекаем данные о каждом компоненте для формата кодирования изображения YUV
        val yBuffer = image.planes[0].buffer
        val uBuffer = image.planes[1].buffer
        val vBuffer = image.planes[2].buffer

        val ySize = yBuffer.remaining()
        val uSize = uBuffer.remaining()
        val vSize = vBuffer.remaining()

        val yvuAllBytes = ByteArray(ySize + uSize + vSize)

        yBuffer.get(yvuAllBytes, 0, ySize)
        vBuffer.get(yvuAllBytes, ySize, vSize)
        uBuffer.get(yvuAllBytes, ySize + vSize, uSize)

        val yuvImage = YuvImage(yvuAllBytes, ImageFormat.NV21, image.width, image.height, null)

        val out = ByteArrayOutputStream()

        yuvImage.compressToJpeg(Rect(0, 0, yuvImage.width, yuvImage.height), QUALITY_COMPRESS, out)

        val jpegImageBytes = out.toByteArray()

        return BitmapFactory.decodeByteArray(jpegImageBytes, 0, jpegImageBytes.size)
    }

    companion object {
        private const val QUALITY_COMPRESS = 100

        private const val RIGHT_ANGLE = 90f
    }
}
