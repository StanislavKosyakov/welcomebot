package com.itrocket.welcomebot.facenetdetection.faceRecognition

import android.util.Size
import android.view.Surface
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetectorOptions
import com.itrocket.welcomebot.facenetdetection.faceRecognition.domain.FaceRecognitionInteractor
import com.itrocket.welcomebot.facenetdetection.faceRecognition.domain.boundary.FaceRecognitionDomain
import com.itrocket.welcomebot.facenetdetection.faceRecognition.service.FaceAnalyzer
import com.welcomebot.facerecognition.extractor.FaceExtractor
import com.welcomebot.facerecognition.model.FaceNetModel
import kotlinx.coroutines.CoroutineScope
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.support.common.FileUtil
import java.util.concurrent.Executors

object FaceRecognitionModule {

    private const val CAMERA_RESOLUTION_WIDTH = 720
    private const val CAMERA_RESOLUTION_HEIGHT = 960

    val module = module {

        factory {
            val options = FaceDetectorOptions.Builder()
                .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                .build()

            FaceDetection.getClient(options)
        }

        factory {
            FaceExtractor(get(), get())
        }

        factory { (scope: CoroutineScope) ->
            FaceAnalyzer(scope, get())
        }

        factory { (scope: CoroutineScope) ->
            ImageAnalysis.Builder()
                .setTargetRotation(Surface.ROTATION_0)
                .setTargetResolution(Size(CAMERA_RESOLUTION_WIDTH, CAMERA_RESOLUTION_HEIGHT))
                .build()
                .also {
                    it.setAnalyzer(
                        Executors.newSingleThreadExecutor(),
                        get<FaceAnalyzer> { parametersOf(scope) })
                }
        }

        factory {
            CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build()
        }

        factory {
            val interpreterOptions = Interpreter.Options().apply {
                setNumThreads(4)
            }

            Interpreter(
                FileUtil.loadMappedFile(get(), "facenet_int8_quant.tflite"),
                interpreterOptions
            )
        }

        factory<FaceRecognitionDomain> {
            FaceRecognitionInteractor(get(), FaceNetModel(get()), get())
        }
    }
}