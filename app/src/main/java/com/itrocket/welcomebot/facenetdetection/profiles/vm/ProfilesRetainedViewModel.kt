package com.itrocket.welcomebot.facenetdetection.profiles.vm

import androidx.lifecycle.MutableLiveData
import com.itrocket.structure.vm.BaseRetainedViewModel
import com.itrocket.welcomebot.facenetdetection.profiles.domain.boudary.ProfilesDomain
import com.itrocket.welcomebot.facenetdetection.profiles.domain.models.Profile
import com.itrocket.welcomebot.facenetdetection.profiles.ui.ProfilesFragmentDirections
import com.itrocket.welcomebot.facenetdetection.profiles.vm.boundary.ProfilesViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.core.inject

class ProfilesRetainedViewModel : BaseRetainedViewModel(), ProfilesViewModel {

    private val domain: ProfilesDomain by inject()

    override val profiles = MutableLiveData<List<Profile>>()

    init {
        launch {
            domain.getProfiles().collect {
                profiles.postValue(it)
            }
        }
    }

    override fun onProfileClick(profile: Profile) {
        navigate(ProfilesFragmentDirections.toProfile(profile))
    }

    override fun onCreateProfileClick() {
        navigate(ProfilesFragmentDirections.toProfile())
    }
}