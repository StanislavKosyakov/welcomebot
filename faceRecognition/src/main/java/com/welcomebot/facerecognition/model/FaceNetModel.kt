package com.welcomebot.facerecognition.model

import android.graphics.Bitmap
import org.tensorflow.lite.Interpreter
import java.nio.ByteBuffer
import java.nio.ByteOrder
import kotlin.math.pow
import kotlin.math.sqrt

class FaceNetModel(
    private val interpreter: Interpreter
) {
    private val imgSize = 160

    private fun runFaceNet(inputs: Any): Array<FloatArray> {
        val outputs = Array(1) { FloatArray(128) }
        interpreter.run(inputs, outputs)

        return outputs
    }

    private fun convertBitmapToBuffer(image: Bitmap): ByteBuffer {
        val imageByteBuffer = ByteBuffer.allocateDirect(1 * imgSize * imgSize * 3 * 4)
        imageByteBuffer.order(ByteOrder.nativeOrder())

        val resizedImage = Bitmap.createScaledBitmap(image, imgSize, imgSize, true)

        for (x in 0 until imgSize) {
            for (y in 0 until imgSize) {
                val pixelValue = resizedImage.getPixel(x, y)

                imageByteBuffer.apply {
                    putFloat((((pixelValue shr 16 and 0xFF) - 128f) / 128f))
                    putFloat((((pixelValue shr 8 and 0xFF) - 128f) / 128f))
                    putFloat((((pixelValue and 0xFF) - 128f) / 128f))
                }
            }
        }

        return imageByteBuffer
    }

    /**
     * Сравнивает два векторных представления изображения и получает коэффицент схожести
     *
     * @param face1 FloatArray векторное представление первого лица
     * @param face2 FloatArray векторное представление второго лица
     *
     * @return Float коэффицент схожести
     */
    fun cosineSimilarity(face1: FloatArray, face2: FloatArray): Float {
        var dotProduct = 0.0f
        var mag1 = 0.0f
        var mag2 = 0.0f

        for (i in face1.indices) {
            dotProduct += (face1[i] * face2[i])
            mag1 += face1[i].toDouble().pow(2.0).toFloat()
            mag2 += face2[i].toDouble().pow(2.0).toFloat()
        }

        mag1 = sqrt(mag1)
        mag2 = sqrt(mag2)

        return dotProduct / (mag1 * mag2)
    }

    /**
     * Для изображения только с лицом формирует его векторное представление,
     * т.о. сравнивая два вектора полученных из изображения можно определить принадлежит ли лицо одному человеку.
     *
     * @param faceImage Bitmap - только изображение с лицом
     * @return FloatArray - вектор лица
     */
    fun getFaceEmbedding(faceImage: Bitmap): FloatArray {
        return runFaceNet(convertBitmapToBuffer(faceImage))[0]
    }

    /**
     * Рассчитывает векторное представления для каждого изображения
     *
     * @param imageFaces Map<String, Bitmap> изображения с лицами
     * @return Map<String, FloatArray>название изображения и его векторное представление
     */
    suspend fun processImageFaces(imageFaces: Map<String, Bitmap>): Map<String, FloatArray> =
        imageFaces.mapValues {
            val face = it.component2()

            getFaceEmbedding(face)
        }

    companion object {
        const val SIMILARITY_THRESHOLD_FACTOR = 0.8
    }
}