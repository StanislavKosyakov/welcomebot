package com.itrocket.structure.vm.boundary

import androidx.lifecycle.LiveData
import com.itrocket.structure.utils.Navigation

interface BaseViewModel {

    val navigation: LiveData<Navigation>
}