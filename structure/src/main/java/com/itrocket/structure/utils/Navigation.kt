package com.itrocket.structure.utils

import androidx.navigation.NavDirections
import com.itrocket.structure.exception.BaseException

sealed class Navigation {
    data class To(val navDirections: NavDirections) : Navigation()

    object Back : Navigation()

    data class ToException(val baseException: BaseException) : Navigation()
}