package com.itrocket.welcomebot.facenetdetection.profile.domain

import android.content.Context
import android.graphics.Bitmap
import com.itrocket.welcomebot.facenetdetection.profile.data.db.dao.ProfileDao
import com.itrocket.welcomebot.facenetdetection.profile.data.db.entity.ProfileEntity
import com.itrocket.welcomebot.facenetdetection.profile.domain.boundary.ProfileDomain
import com.welcomebot.facerecognition.extractor.FaceExtractor

class ProfileInteractor(
    private val faceExtractor: FaceExtractor,
    private val profileDao: ProfileDao,
    private val context: Context
) : ProfileDomain {

    override suspend fun extractFace(photo: Bitmap): Bitmap =
        faceExtractor.getFace(photo)

    override suspend fun createProfile(name: String, face: Bitmap) {
        val faceAbsolutePath = saveFile(context, face, name)

        val profileEntity = ProfileEntity(name = name, faceAbsoluteFilePath = faceAbsolutePath)

        profileDao.insert(profileEntity)
    }

    private fun saveFile(
        context: Context,
        bitmapImage: Bitmap,
        imageFileName: String
    ): String {
        context.openFileOutput(imageFileName + MIME_TYPE_JPG, Context.MODE_PRIVATE).use { fos ->
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos)
        }

        return context.filesDir.absolutePath + "/" + imageFileName + MIME_TYPE_JPG
    }

    override suspend fun updateProfile(id: Int, newName: String?, newFace: Bitmap?) {
        val entity = profileDao.get(id)

        if (newName != null) {
            entity.name = newName
        }

        if (newFace != null) {
            entity.faceAbsoluteFilePath = saveFile(context, newFace, newName ?: entity.name)
        }

        profileDao.insert(entity)
    }

    companion object {
        private const val MIME_TYPE_JPG = ".jpg"
    }
}