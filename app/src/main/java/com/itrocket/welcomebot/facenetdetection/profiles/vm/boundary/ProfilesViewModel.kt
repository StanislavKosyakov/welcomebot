package com.itrocket.welcomebot.facenetdetection.profiles.vm.boundary

import androidx.lifecycle.LiveData
import com.itrocket.structure.vm.boundary.BaseViewModel
import com.itrocket.welcomebot.facenetdetection.profiles.domain.models.Profile

interface ProfilesViewModel : BaseViewModel {

    val profiles: LiveData<List<Profile>>

    fun onProfileClick(profile: Profile)

    fun onCreateProfileClick()
}