package com.itrocket.welcomebot.facenetdetection.infrastructure.utils

import android.graphics.Bitmap
import android.graphics.Matrix

/**
 * Поворачивает Bitmap на угол переданный в параметре
 *
 * @receiver Bitmap - исходное изображение
 * @param angle Float - угол поворота
 * @return Bitmap - выходное изображение
 */
fun Bitmap.rotate(angle: Float): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(angle)

    return Bitmap.createBitmap(this, 0, 0, this.width, this.height, matrix, false)
}