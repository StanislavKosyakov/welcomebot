package com.welcomebot.facerecognition.extractor

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Rect
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.FaceDetector
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class FaceExtractor(private val faceDetector: FaceDetector, private val context: Context) {

    /**
     * Находит лицо и обрезает картинку по его границам если на фото нету ни одного лица упадет с эксепшеном
     *
     * @param inputImage Bitmap входная картинка с лицом
     * @return Bitmap только картинка с лицом
     * @throws FaceNotFoundException - если лицо на фото не было найдено
     */
    suspend fun getFace(inputImage: Bitmap): Bitmap {
        val boundingBox = getFirstFaceBoundingBox(inputImage)

        return cropRectFromBitmap(inputImage, boundingBox)
    }

    private fun cropRectFromBitmap(source: Bitmap, rect: Rect): Bitmap {
        var width = rect.width()
        var height = rect.height()

        if ((rect.left + width) > source.width) {
            width = source.width - rect.left
        }

        if ((rect.top + height) > source.height) {
            height = source.height - rect.top
        }

        return Bitmap.createBitmap(source, rect.left, rect.top, width, height)
    }

    private suspend fun getFirstFaceBoundingBox(faceImage: Bitmap): Rect =
        suspendCoroutine { continuation ->
            val inputImage = InputImage.fromBitmap(faceImage, 0)

            faceDetector.process(inputImage).addOnSuccessListener { faces ->
                if (faces.isEmpty()) {
                    val exception = FaceNotFoundException(context)

                    continuation.resumeWithException(exception)
                } else {
                    val firstRect = faces.first().boundingBox

                    continuation.resume(firstRect.getSaveRect())
                }
            }.addOnFailureListener {
                continuation.resumeWithException(it)
            }
        }

    /**
     * Иногда firebase может вернуть Rect в котором left или top с координатой меньше нуля и
     * это станет причиной ошибки при создании Bitmap
     */
    private fun Rect.getSaveRect(): Rect {
        val saveRect = Rect(this)

        if (saveRect.left < 0) {
            saveRect.left = 0
        }

        if (saveRect.top < 0) {
            saveRect.top = 0
        }

        return saveRect
    }
}