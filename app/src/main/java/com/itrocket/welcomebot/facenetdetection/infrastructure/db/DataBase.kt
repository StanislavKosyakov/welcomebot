package com.itrocket.welcomebot.facenetdetection.infrastructure.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.itrocket.welcomebot.facenetdetection.profile.data.db.dao.ProfileDao
import com.itrocket.welcomebot.facenetdetection.profile.data.db.entity.ProfileEntity

@Database(
    entities = [
        ProfileEntity::class
    ],
    version = 1
)

abstract class DataBase : RoomDatabase() {

    abstract fun profileDao(): ProfileDao
}