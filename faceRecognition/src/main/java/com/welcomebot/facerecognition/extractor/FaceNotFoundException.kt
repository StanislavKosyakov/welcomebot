package com.welcomebot.facerecognition.extractor

import android.content.Context
import com.itrocket.structure.exception.BaseException
import com.welcomebot.facerecognition.R

class FaceNotFoundException(context: Context) :
    BaseException(
        context.getString(R.string.face_not_found_error_title),
        context.getString(R.string.face_not_found_error_message)
    )