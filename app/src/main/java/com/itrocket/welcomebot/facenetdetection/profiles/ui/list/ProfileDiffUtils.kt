package com.itrocket.welcomebot.facenetdetection.profiles.ui.list

import androidx.recyclerview.widget.DiffUtil
import com.itrocket.welcomebot.facenetdetection.profiles.domain.models.Profile

class ProfileDiffUtils : DiffUtil.ItemCallback<Profile>() {
    override fun areItemsTheSame(oldItem: Profile, newItem: Profile): Boolean =
        oldItem.name == newItem.name

    override fun areContentsTheSame(oldItem: Profile, newItem: Profile): Boolean =
        oldItem == newItem
}