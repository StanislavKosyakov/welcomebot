package com.itrocket.welcomebot.facenetdetection.profile.vm.boundary

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.LiveData
import com.itrocket.structure.vm.boundary.BaseViewModel

interface ProfileViewModel : BaseViewModel {

    val photo: LiveData<Bitmap?>

    val name: LiveData<String>

    fun onPhotoPicked(uri: Uri)

    fun onSaveClick()

    fun onNameChanged(name: String)
}