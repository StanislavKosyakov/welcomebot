package com.itrocket.welcomebot.facenetdetection.profiles.domain.boudary

import com.itrocket.welcomebot.facenetdetection.profiles.domain.models.Profile
import kotlinx.coroutines.flow.Flow

interface ProfilesDomain {

    /**
     * Возвращает источник списка добавленных профилей
     *
     * @return List<Profile> -источник списка добавленных профилей
     */
    suspend fun getProfiles(): Flow<List<Profile>>
}